class ProcessCreateUpdateAPIViewMixin:
    def _process_view_action(self):
        serializer = self.get_serializer(data=self.request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return serializer, headers

    def process_serializer_create(self):
        return self._process_view_action()

    def process_serializer_update(self):
        return self._process_view_action()
