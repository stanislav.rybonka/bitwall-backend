from django.urls import include
from django.urls import path
from rest_framework.routers import SimpleRouter

from wallets import views as wallets_views


router = SimpleRouter()

router.register('wallets', wallets_views.WalletModelViewSet, basename='wallets')
router.register('transactions', wallets_views.TransactionModelViewSet, basename='transactions')

urlpatterns = [
    path('', include(router.urls)),
    path('statistics/', wallets_views.StatisticsAPIView.as_view(), name='statistics')
]
