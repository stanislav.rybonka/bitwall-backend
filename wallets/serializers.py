from django.conf import settings
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy as _
from rest_framework import serializers

from wallets import models as wallets_models
from wallets.services import WalletService


class WalletCreateModelSerializer(serializers.ModelSerializer):
    pin_code1 = serializers.CharField(write_only=True, help_text=_('Pin code'))
    pin_code2 = serializers.CharField(write_only=True, help_text=_('Pin code confirmation'))

    class Meta:
        model = wallets_models.Wallet
        fields = ('currency', 'label', 'pin_code1', 'pin_code2')
        SWAGGER_REF_MODEL_NAME = 'WalletCreateObject'
        ref_name = SWAGGER_REF_MODEL_NAME

    def validate(self, attrs):
        if attrs['pin_code1'] != attrs['pin_code2']:
            raise serializers.ValidationError(_("The two pin codes fields didn't match."))

        attrs.pop('pin_code2', None)

        if 'request' in self.context:
            current_user = self.context['request'].user

            wallets_count = current_user.wallets.count()

            if wallets_count > settings.WALLET_MAX_AMOUNT:
                raise serializers.ValidationError(_("Reached max wallets limit."))

        return attrs

    def create(self, validated_data):
        private_key_bin, private_key_hex, _ = self.Meta.model.generate_private_key()
        public_key = self.Meta.model.generate_public_key(private_key_bin)
        address = self.Meta.model.generate_address(public_key)
        raw_pin_code = validated_data.pop('pin_code1', None)

        validated_data['private_key'] = private_key_hex
        validated_data['public_key'] = public_key
        validated_data['address'] = address
        validated_data['pin_code'] = make_password(raw_pin_code)

        wallet = super().create(validated_data)
        wallet.grant_initial_coins()
        return wallet


class WalletDetailModelSerializer(serializers.ModelSerializer):
    total_btc = serializers.SerializerMethodField()
    total_usd = serializers.SerializerMethodField()

    wallet_service = WalletService

    class Meta:
        model = wallets_models.Wallet
        fields = ('id', 'currency', 'address', 'total_btc', 'total_usd')
        SWAGGER_REF_MODEL_NAME = 'WalletDetailObject'
        ref_name = SWAGGER_REF_MODEL_NAME

    def get_total_btc(self, wallet):
        if getattr(wallet, 'balances', None):
            total_btc = sum([transaction.total_btc for transaction in wallet.balances])
        else:
            total_btc = wallet.in_btc()
        return total_btc

    def get_total_usd(self, wallet):
        if getattr(wallet, 'balances', None):
            total_usd = sum([transaction.total_usd for transaction in wallet.balances])
        else:
            total_usd = wallet.in_usd()
        return total_usd


class WalletDetailShortModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = wallets_models.Wallet
        fields = ('id', 'currency', 'address')
        ref_name = None


class WalletListModelSerializer(WalletDetailModelSerializer):
    ...


class TransactionCreateModelSerializer(serializers.ModelSerializer):
    wallet_from = serializers.PrimaryKeyRelatedField(
        queryset=wallets_models.Wallet.objects.all(),
        label=_('wallet from'),
    )
    wallet_to = serializers.PrimaryKeyRelatedField(
        queryset=wallets_models.Wallet.objects.all(),
        label=_('wallet_to'),
    )

    class Meta:
        model = wallets_models.Transaction
        fields = ('wallet_from', 'wallet_to', 'amount')
        SWAGGER_REF_MODEL_NAME = 'TransactionCreateObject'
        ref_name = SWAGGER_REF_MODEL_NAME

    def validate(self, attrs):
        wallet_from = attrs.get('wallet_from')
        amount_to_send = attrs.get('amount')
        current_balance = wallet_from.get_total_unspent_transactions_balance()

        if amount_to_send > current_balance:
            raise serializers.ValidationError({
                'amount': _('Too large amount specified. Not enough of funds.')
            })

        return super().validate(attrs)

    def save(self, **kwargs):
        wallet_from = self.validated_data.get('wallet_from')
        wallet_to = self.validated_data.get('wallet_to')
        amount = self.validated_data.get('amount')

        if 'request' in self.context:
            current_user = self.context['request'].user
            wallet_from.transfer_coins_to_receiver(
                amount, wallet_to, current_user,
            )


class TransactionDetailSerializer(serializers.ModelSerializer):
    sender = WalletDetailShortModelSerializer()
    receiver = WalletDetailShortModelSerializer()

    class Meta:
        model = wallets_models.Transaction
        fields = (
            'id', 'amount', 'status', 'network_fee', 'platform_fee', 'sender', 'receiver',
            'signature', 'tx_number',
        )
        SWAGGER_REF_MODEL_NAME = 'TransactionDetailObject'
        ref_name = SWAGGER_REF_MODEL_NAME


class TransactionListSerializer(TransactionDetailSerializer):
    ...


class StatisticsSerializer(serializers.Serializer):
    profit_satoshi = serializers.DecimalField(read_only=True, max_digits=16, decimal_places=6)
    profit_btc = serializers.DecimalField(read_only=True, max_digits=16, decimal_places=6)
    profit_usd = serializers.DecimalField(read_only=True, max_digits=16, decimal_places=6)
    total_transactions = serializers.IntegerField(read_only=True)

    class Meta:
        SWAGGER_REF_MODEL_NAME = 'StatisticsObject'
        ref_name = SWAGGER_REF_MODEL_NAME
