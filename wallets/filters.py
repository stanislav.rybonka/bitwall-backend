from django.db.models import Q
from django_filters import rest_framework as filters

from wallets.models import Transaction


class TransactionFilter(filters.FilterSet):
    wallet_address = filters.CharFilter(method='filter_by_address')

    def filter_by_address(self, queryset, name, value):
        return queryset.filter(Q(sender__address=value) | Q(receiver__address=value))

    class Meta:
        model = Transaction
        fields = ['wallet_address']
