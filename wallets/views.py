from django.utils.decorators import method_decorator
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins as drf_mixins
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from common.mixins import ProcessCreateUpdateAPIViewMixin
from common.serializers import PaginatedListSerializer
from common.utils import get_default_schema_responses
from wallets import models as wallets_models
from wallets import serializers as wallets_serializers
from wallets.filters import TransactionFilter


@method_decorator(
    name='create', decorator=swagger_auto_schema(
        request_body=wallets_serializers.WalletCreateModelSerializer,
        responses=get_default_schema_responses({
            '201': wallets_serializers.WalletDetailModelSerializer,
        }),
    )
)
@method_decorator(
    name='list', decorator=swagger_auto_schema(
        responses=get_default_schema_responses({
            '200': PaginatedListSerializer(
                result_serializer=wallets_serializers.WalletListModelSerializer,
            ),
        }, exclude=['400', '401', '403']),
    )
)
@method_decorator(
    name='retrieve', decorator=swagger_auto_schema(
        responses=get_default_schema_responses({
            '200': wallets_serializers.WalletDetailModelSerializer,
        }, exclude=['400', '401', '403']),
    )
)
class WalletModelViewSet(
    drf_mixins.CreateModelMixin,
    drf_mixins.RetrieveModelMixin,
    drf_mixins.ListModelMixin,
    drf_mixins.DestroyModelMixin,
    ProcessCreateUpdateAPIViewMixin,
    GenericViewSet,
):
    queryset = wallets_models.Wallet.objects.annotate_with_balances()
    serializer_class = wallets_serializers.WalletCreateModelSerializer
    permission_classes = (IsAuthenticated,)

    serializers_map = {
        'create': wallets_serializers.WalletCreateModelSerializer,
        'list': wallets_serializers.WalletListModelSerializer,
        'retrieve': wallets_serializers.WalletDetailModelSerializer,

    }

    def create(self, request, *args, **kwargs):
        serializer, headers = self.process_serializer_create()
        serializer = wallets_serializers.WalletDetailModelSerializer(serializer.instance)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer_class(self):
        return self.serializers_map.get(self.action)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


@method_decorator(
    name='create', decorator=swagger_auto_schema(
        request_body=wallets_serializers.TransactionCreateModelSerializer,
        responses=get_default_schema_responses({
            '201': wallets_serializers.WalletDetailModelSerializer,
        }),
    )
)
@method_decorator(
    name='list', decorator=swagger_auto_schema(
        responses=get_default_schema_responses({
            '200': PaginatedListSerializer(
                result_serializer=wallets_serializers.TransactionListSerializer,
            ),
        }, exclude=['400', '401', '403']),
    )
)
@method_decorator(
    name='retrieve', decorator=swagger_auto_schema(
        responses=get_default_schema_responses({
            '200': wallets_serializers.TransactionDetailSerializer,
        }, exclude=['400', '401', '403']),
    )
)
class TransactionModelViewSet(
    drf_mixins.CreateModelMixin,
    drf_mixins.RetrieveModelMixin,
    drf_mixins.ListModelMixin,
    ProcessCreateUpdateAPIViewMixin,
    GenericViewSet,
):
    queryset = wallets_models.Transaction.objects.all()
    serializer_class = wallets_serializers.TransactionCreateModelSerializer
    filterset_class = TransactionFilter
    permission_classes = (IsAuthenticated,)

    serializers_map = {
        'create': wallets_serializers.TransactionCreateModelSerializer,
        'list': wallets_serializers.TransactionListSerializer,
        'retrieve': wallets_serializers.TransactionDetailSerializer,
    }

    def create(self, request, *args, **kwargs):
        serializer, headers = self.process_serializer_create()
        wallet_serializer = wallets_serializers.WalletDetailModelSerializer(
            serializer.validated_data['wallet_from']
        )
        return Response(wallet_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_serializer_class(self):
        return self.serializers_map.get(self.action)

    def get_queryset(self):
        return super().get_queryset().all_for_user(user=self.request.user)


class StatisticsAPIView(APIView):
    permission_classes = [IsAdminUser]

    @swagger_auto_schema(responses=get_default_schema_responses(
        {'200': wallets_serializers.StatisticsSerializer}, exclude=['400'])
    )
    def get(self, request, **kwargs):
        statistics_data = wallets_models.Transaction.objects.get_statistics()
        return Response(statistics_data)
