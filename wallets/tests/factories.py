import factory
from factory import DjangoModelFactory

from wallets.models import Transaction
from wallets.models import Wallet


class WalletFactory(DjangoModelFactory):
    creator = factory.SubFactory('users.tests.factories.UserFactory')
    address = factory.Faker('pystr')
    public_key = factory.Faker('md5')
    private_key = factory.Faker('md5')
    label = factory.Faker('word')

    class Meta:
        model = Wallet


class TransactionFactory(DjangoModelFactory):
    sender = factory.SubFactory(WalletFactory)
    receiver = factory.SubFactory(WalletFactory)
    amount = factory.Faker(
        'pydecimal', positive=True,
        left_digits=3, right_digits=3,
    )
    network_fee = factory.Faker(
        'pydecimal', positive=True,
        left_digits=3, right_digits=3,
    )
    platform_fee = factory.Faker(
        'pydecimal', positive=True,
        left_digits=3, right_digits=3,
    )
    signature = factory.Faker('sha256')
    tx_number = factory.Faker('md5')

    class Meta:
        model = Transaction
