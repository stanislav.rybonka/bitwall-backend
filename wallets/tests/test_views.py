import factory
import pytest
from rest_framework import status
from rest_framework.reverse import reverse

from wallets.models import Transaction
from wallets.models import Wallet
from wallets.tests.factories import TransactionFactory
from wallets.tests.factories import WalletFactory


pytestmark = pytest.mark.django_db


class TestWalletModelViewSet:
    wallets_list_url = reverse('api:wallets:wallets-list')
    wallets_detail_url = 'api:wallets:wallets-detail'

    def test_create_action(self, api_client, user):
        api_client.force_authenticate(user)

        pin_code = factory.Faker('password').generate()

        payload = {
            'label': factory.Faker('word').generate(),
            'pin_code1': pin_code,
            'pin_code2': pin_code,
        }
        response = api_client.post(self.wallets_list_url, data=payload)

        assert response.status_code == status.HTTP_201_CREATED

        wallet = Wallet.objects.get(creator=user)

        assert str(wallet.id) == response.data['id']
        assert wallet.address == response.data['address']
        assert wallet.currency == response.data['currency']

    def test_list_action(self, api_client, user):
        api_client.force_authenticate(user)
        WalletFactory.create_batch(20, creator=user)

        response = api_client.get(self.wallets_list_url)

        assert response.status_code == status.HTTP_200_OK
        assert response.data['count'] == 20

    def test_retrieve_action(self, api_client, user):
        api_client.force_authenticate(user)
        wallet = WalletFactory()

        response = api_client.get(
            reverse(self.wallets_detail_url, args=(wallet.id,))
        )

        assert response.status_code == status.HTTP_200_OK
        assert response.data['id'] == str(wallet.id)
        assert response.data['currency'] == wallet.currency
        assert response.data['address'] == wallet.address


class TestTransactionModelViewSet:
    transactions_list_url = reverse('api:wallets:transactions-list')
    transactions_detail_url = 'api:wallets:transactions-detail'

    def test_create_action(self, api_client, user):
        api_client.force_authenticate(user)

        wallet_from = WalletFactory(creator=user)
        wallet_to = WalletFactory()

        wallet_from.grant_initial_coins()

        payload = {
            'wallet_from': str(wallet_from.id),
            'wallet_to': str(wallet_to.id),
            'amount': '1000',
        }

        response = api_client.post(self.transactions_list_url, data=payload)

        assert response.status_code == status.HTTP_201_CREATED

    def test_list_action(self, api_client, user):
        api_client.force_authenticate(user)

        wallet = WalletFactory(creator=user)
        TransactionFactory.create_batch(10, sender=wallet)

        response = api_client.get(self.transactions_list_url)

        assert response.status_code == status.HTTP_200_OK
        assert response.data['count'] == 10

    def test_retrieve_action(self, api_client, user):
        api_client.force_authenticate(user)

        wallet = WalletFactory(creator=user)
        transaction = TransactionFactory(sender=wallet)

        response = api_client.get(
            reverse(self.transactions_detail_url, args=(transaction.id,))
        )

        assert response.status_code == status.HTTP_200_OK
        assert response.data['id'] == str(transaction.id)


class TestStatisticsAPIView:
    def test_get_method(self, api_client, user):
        user.is_superuser = True
        user.is_staff = True
        user.save()
        api_client.force_authenticate(user)

        TransactionFactory.create_batch(20)

        statistic_url = reverse('api:wallets:statistics')

        response = api_client.get(statistic_url)

        assert response.status_code == status.HTTP_200_OK

        assert response.data['profit_satoshi']
        assert response.data['profit_btc']
        assert response.data['profit_usd']
        assert response.data['total_transactions'] == Transaction.objects.count()
