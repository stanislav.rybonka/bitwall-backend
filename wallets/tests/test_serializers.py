import decimal

import factory
import pytest

from wallets import serializers as wallets_serializers
from wallets.tests.factories import TransactionFactory
from wallets.tests.factories import WalletFactory


pytestmark = pytest.mark.django_db


class TestWalletCreateModelSerializer:

    def test_to_representation(self):
        pin_code = factory.Faker('password').generate()
        payload = {
            'pin_code1': pin_code,
            'pin_code2': pin_code,
            'currency': 'btc',
            'label': factory.Faker('word').generate(),
        }

        serializer = wallets_serializers.WalletCreateModelSerializer(payload)

        assert payload['currency'] == serializer.data['currency']
        assert payload['label'] == serializer.data['label']


class TestWalletDetailModelSerializer:
    def test_to_representation(self):
        wallet = WalletFactory()

        serializer = wallets_serializers.WalletDetailModelSerializer(instance=wallet)
        data = serializer.data

        assert str(wallet.id) == data['id']
        assert wallet.currency == data['currency']
        assert wallet.address == data['address']
        assert 'total_btc' in data
        assert 'total_usd' in data


class TestTransactionCreateModelSerializer:
    def test_to_representation(self):
        wallet_from = WalletFactory()
        wallet_to = WalletFactory()

        payload = {
            'wallet_from': wallet_from,
            'wallet_to': wallet_to,
            'amount': factory.Faker('pydecimal', positive=True, left_digits=3, right_digits=3).generate()
        }

        serializer = wallets_serializers.TransactionCreateModelSerializer(
            payload,
        )
        data = serializer.data

        assert str(wallet_from.id) == str(data['wallet_from'])
        assert str(wallet_to.id) == str(data['wallet_to'])
        assert 'amount' in data


class TestTransactionDetailSerializer:
    def test_to_representation(self):
        transaction = TransactionFactory()
        serializer = wallets_serializers.TransactionDetailSerializer(
            instance=transaction,
        )
        data = serializer.data

        assert str(transaction.id) == data['id']
        assert transaction.amount == decimal.Decimal(data['amount'])
        assert transaction.network_fee == decimal.Decimal(data['network_fee'])
        assert transaction.platform_fee == decimal.Decimal(data['platform_fee'])
        assert transaction.signature == data['signature']
        assert transaction.tx_number == data['tx_number']


class TestStatisticsSerializer:
    def test_to_representation(self):
        payload = {
            'profit_satoshi': '1000.000000',
            'profit_btc': '0.000001',
            'profit_usd': '1.000000',
            'total_transactions': 55,
        }

        serializer = wallets_serializers.StatisticsSerializer(payload)

        data = serializer.data

        assert data['profit_satoshi'] == payload['profit_satoshi']
        assert data['profit_btc'] == payload['profit_btc']
        assert data['profit_usd'] == payload['profit_usd']
        assert data['total_transactions'] == payload['total_transactions']
