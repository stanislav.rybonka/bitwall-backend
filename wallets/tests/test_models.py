import decimal

import pytest
from django.conf import settings

from users.tests.factories import UserFactory
from wallets.models import Transaction
from wallets.models import Wallet
from wallets.tests.factories import TransactionFactory
from wallets.tests.factories import WalletFactory


pytestmark = pytest.mark.django_db


class TestWallet:
    def test_grant_initial_coins_method(self):
        wallet = WalletFactory()
        wallet.grant_initial_coins()

        initial_transaction = Transaction.objects.first()

        assert initial_transaction
        assert initial_transaction.parent is None
        assert initial_transaction.sender is None
        assert initial_transaction.amount == settings.WALLET_INITIAL_VALUE

    def test_get_total_unspent_transactions_balance_method(self):
        wallet = WalletFactory()
        TransactionFactory.create_batch(10, receiver=wallet)

        assert wallet.get_total_unspent_transactions_balance()

    def test_in_btc_method(self):
        wallet = WalletFactory()
        assert not wallet.in_btc()

    def test_in_usd_method(self):
        wallet = WalletFactory()
        assert not wallet.in_usd()

    def test_transfer_coins_to_receiver_method(self):
        user = UserFactory()
        wallet_from = WalletFactory(creator=user)
        wallet_to = WalletFactory(creator=user)

        wallet_from.grant_initial_coins()

        wallet_from.transfer_coins_to_receiver(
            to_send_amount=decimal.Decimal('10'), receiver=wallet_to,
            current_user=user,
        )

        assert wallet_from.as_sender_transactions.count() == 1
        assert wallet_to.as_receiver_transactions.count() == 1
        assert wallet_from.as_receiver_transactions.count() == 2

    def test_get_unspent_source_transactions_method(self):
        wallet = WalletFactory()
        TransactionFactory.create_batch(10, receiver=wallet)

        transactions_list = wallet.get_unspent_source_transactions(decimal.Decimal('100'))

        assert len(transactions_list) == 1

        transactions_list = wallet.get_unspent_source_transactions(decimal.Decimal('200000000'))

        assert len(transactions_list) >= 1

    def test_generate_wallet_keys(self):
        private_key_bin, private_key_hex, WIF = Wallet.generate_private_key()
        public_key = Wallet.generate_public_key(private_key_bin)
        address = Wallet.generate_address(public_key)

        assert private_key_bin
        assert private_key_bin
        assert WIF
        assert public_key
        assert address


class TestTransaction:

    def test_is_system_zero_created_property(self):
        transaction = TransactionFactory()
        assert transaction.is_system_zero_created

    def test_set_signature_method(self):
        transaction = TransactionFactory()
        transaction.set_signature()
        assert transaction.signature

    def test_include_platform_fee_method(self):
        user = UserFactory()
        transaction = TransactionFactory()
        assert Transaction.include_platform_fee(user, transaction.receiver)

    def test_get_platform_fee_method(self):
        platform_fee = Transaction.get_platform_fee(decimal.Decimal('4000'))
        assert platform_fee == (settings.PLATFORM_FEE * decimal.Decimal('4000')) / 100
