from django.contrib import admin

from wallets.models import Transaction
from wallets.models import Wallet


@admin.register(Wallet)
class WalletAdmin(admin.ModelAdmin):
    list_display = ('id', 'creator', 'currency', 'address')


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'receiver', 'amount', 'status', 'tx_number')
