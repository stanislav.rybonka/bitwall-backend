import binascii
import hashlib
import os

import base58
import ecdsa
from django.conf import settings
from django.db import models
from django.db.models import Sum
from django.forms.models import model_to_dict
from django.utils.translation import gettext_lazy as _
from model_utils import Choices
from model_utils.models import StatusModel

from common.models import AbstractUUIDModel
from wallets.managers import TransactionQuerySet
from wallets.managers import WalletQueryset
from wallets.services import WalletService


class Wallet(AbstractUUIDModel):
    CURRENCIES = Choices(
        ('btc', _('BTC')),
    )
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name='wallets',
        on_delete=models.CASCADE, verbose_name=_('creator'),
    )
    currency = models.CharField(
        max_length=10, choices=CURRENCIES,
        default=CURRENCIES.btc, verbose_name=_('currency'),
    )
    address = models.CharField(max_length=35, verbose_name=_('address'))
    public_key = models.TextField(verbose_name=_('public key'))
    private_key = models.TextField(verbose_name=_('private key'))
    label = models.CharField(verbose_name=_('label'), max_length=50, blank=True)
    pin_code = models.CharField(verbose_name=_('pin code'), max_length=128, blank=True)

    objects = WalletQueryset.as_manager()
    wallet_service = WalletService

    class Meta:
        verbose_name = _('wallet')
        verbose_name_plural = _('wallets')
        ordering = ('-created',)

    def __str__(self):
        return f"{self.creator}: {self.id}"

    def _process_instance_pre_save(self, **data):
        transaction = Transaction(**data)
        transaction.set_signature()
        transaction.set_double_sha256_hash(transaction.get_payload_from_instance())
        return transaction

    def grant_initial_coins(self):
        transaction = Transaction.objects.create(receiver=self, amount=settings.WALLET_INITIAL_VALUE)
        instance_payload = transaction.get_payload_from_instance()
        transaction.set_double_sha256_hash(instance_payload)
        transaction.save(update_fields=['tx_number'])

    def get_total_unspent_transactions_balance(self):
        unspent_balance = self.as_receiver_transactions.filter(status=Transaction.STATUS.unspent).aggregate(
            total=Sum('amount')
        )
        return unspent_balance.get('total') or 0

    def in_btc(self):
        return self.wallet_service.convert_satoshi_to_btc(self.get_total_unspent_transactions_balance())

    def in_usd(self):
        return self.wallet_service.convert_satoshi_to_usd(self.get_total_unspent_transactions_balance())

    def transfer_coins_to_receiver(self, to_send_amount, receiver, current_user):
        unspent_source_transactions = self.get_unspent_source_transactions(to_send_amount)

        send_total_per_request_amount = to_send_amount

        unspent_input_transaction_list = []

        for unspent_source_transaction in unspent_source_transactions:

            if unspent_source_transaction.amount >= send_total_per_request_amount:
                total_per_transaction_amount = send_total_per_request_amount

            else:
                total_per_transaction_amount = unspent_source_transaction.amount
                send_total_per_request_amount -= total_per_transaction_amount

            sender_platform_fee = self.calculate_transfer_platform_fee(
                current_user=current_user, amount=total_per_transaction_amount,
                receiver=receiver,
            )

            receiver_transaction = self._process_instance_pre_save(
                parent=unspent_source_transaction, sender=self, receiver=receiver,
                status=Transaction.STATUS.unspent, amount=total_per_transaction_amount,
                network_fee=settings.NETWORK_FEE, platform_fee=sender_platform_fee,
            )

            unspent_input_transaction_list.append(receiver_transaction)

            unspent_source_transaction.mark_as_spent()

            sender_send_back_amount = unspent_source_transaction.amount - total_per_transaction_amount

            if sender_send_back_amount:
                sender_send_back_fee_free_amount = sender_send_back_amount - sender_platform_fee

                sender_transaction = self._process_instance_pre_save(
                    parent=receiver_transaction, sender=receiver_transaction.receiver,
                    receiver=receiver_transaction.sender, amount=sender_send_back_fee_free_amount,
                    network_fee=settings.NETWORK_FEE, status=Transaction.STATUS.unspent,
                )

                unspent_input_transaction_list.append(sender_transaction)

        Transaction.objects.bulk_create(unspent_input_transaction_list)

    def get_unspent_source_transactions(self, total_send_amount):
        unspent_transactions_qs = self.as_receiver_transactions.filter(status=Transaction.STATUS.unspent)

        primary_unspent_transaction = unspent_transactions_qs.filter(amount__gte=total_send_amount).first()

        unspent_transaction_list = []

        if primary_unspent_transaction:
            unspent_transaction_list.append(primary_unspent_transaction)

        else:
            total_reduced_to_succeed_operation = 0

            for unspent_transaction in unspent_transactions_qs:

                if total_send_amount <= total_reduced_to_succeed_operation:
                    break

                total_reduced_to_succeed_operation += unspent_transaction.amount
                unspent_transaction_list.append(unspent_transaction)

        return unspent_transaction_list

    def calculate_transfer_platform_fee(self, current_user, amount, receiver):
        platform_fee_include = Transaction.include_platform_fee(
            current_user=current_user, receiver=receiver,
        )

        platform_fee = 0

        if platform_fee_include:
            platform_fee = Transaction.get_platform_fee(total_amount=amount)

        return platform_fee

    @classmethod
    def generate_private_key(cls):
        private_key_bin = os.urandom(32)
        private_key_hex = binascii.hexlify(private_key_bin).decode()
        sha256a = hashlib.sha256(binascii.unhexlify(private_key_hex)).hexdigest()
        sha256b = hashlib.sha256(binascii.unhexlify(sha256a)).hexdigest()
        WIF = base58.b58encode(binascii.unhexlify(private_key_hex + sha256b[:8]))
        return private_key_bin, private_key_hex, WIF

    @classmethod
    def generate_public_key(cls, private_key_bin):
        singing_key = ecdsa.SigningKey.from_string(private_key_bin, curve=ecdsa.SECP256k1)
        verifying_key = singing_key.get_verifying_key()
        return binascii.hexlify(verifying_key.to_string()).decode()

    @classmethod
    def generate_address(cls, public_key_hex):
        hash160 = cls.make_ripemd160_hash(hashlib.sha256(binascii.unhexlify(public_key_hex)).digest())
        from_public_hash = hash160.digest()
        checksum = hashlib.sha256(hashlib.sha256(from_public_hash).digest()).digest()[:4]
        address = base58.b58encode(from_public_hash + checksum)
        return address.decode()

    @staticmethod
    def make_ripemd160_hash(input):
        output = hashlib.new('ripemd160')
        output.update(input)
        return output


class Transaction(AbstractUUIDModel, StatusModel):
    STATUS = Choices(
        ('unspent', _('Unspent')),
        ('spent', _('Spent')),
    )
    parent = models.ForeignKey(
        'self', related_name='children', on_delete=models.CASCADE,
        verbose_name=_('parent'), null=True,
    )
    sender = models.ForeignKey(
        Wallet, related_name='as_sender_transactions',
        on_delete=models.CASCADE, verbose_name=_('sender'), null=True,
    )
    receiver = models.ForeignKey(
        Wallet, related_name='as_receiver_transactions',
        on_delete=models.CASCADE, verbose_name=_('receiver'),
    )
    amount = models.DecimalField(
        max_digits=16, decimal_places=3,
        verbose_name=_('amount'), help_text=_('Amount is Satoshi'),
    )
    network_fee = models.DecimalField(
        max_digits=6, decimal_places=3,
        default=int, verbose_name=_('network fee'), help_text=_('Amount is Satoshi')
    )
    platform_fee = models.DecimalField(
        max_digits=6, decimal_places=3, default=int,
        verbose_name=_('platform fee'), help_text=_('Amount is Satoshi'),
    )
    confirmations = models.IntegerField(verbose_name=_('confirmations'), default=int)
    signature = models.TextField(verbose_name=_('signature'))
    tx_number = models.CharField(max_length=64, verbose_name=_('tx number'))

    objects = TransactionQuerySet.as_manager()

    class Meta:
        verbose_name = _('transaction')
        verbose_name_plural = _('transactions')
        ordering = ('-created',)

    def __str__(self):
        return str(self.id)

    @property
    def is_system_zero_created(self):
        return not all([self.parent, self.sender])

    def get_payload_from_instance(self):
        return str(model_to_dict(self, fields=('sender__address', 'receiver__address', 'amount')))

    def set_signature(self):
        sk = ecdsa.SigningKey.generate(curve=ecdsa.SECP256k1)
        sig = sk.sign(binascii.unhexlify(self.sender.private_key))
        self.signature = binascii.hexlify(sig).decode()

    def set_double_sha256_hash(self, instance_payload):
        instance_payload_hex = binascii.hexlify(instance_payload.encode())
        sha256_bin = hashlib.sha256(instance_payload_hex).digest()
        sha256_64hex = hashlib.sha256(sha256_bin).hexdigest()
        self.tx_number = sha256_64hex

    def mark_as_spent(self):
        self.status = Transaction.STATUS.spent
        self.save()

    @classmethod
    def include_platform_fee(cls, current_user, receiver):
        return receiver.address not in list(current_user.wallets.values_list('address', flat=True))

    @classmethod
    def get_platform_fee(cls, total_amount):
        return (settings.PLATFORM_FEE * total_amount) / 100
