from django.db.models import Prefetch
from django.db.models import Q
from django.db.models import QuerySet
from django.db.models import Sum

from wallets import models as wallets_models
from wallets.services import WalletService


class WalletQueryset(QuerySet):
    wallet_service = WalletService

    def annotate_with_balances(self):
        wallet_qs = self.prefetch_related(
            Prefetch('as_receiver_transactions', queryset=wallets_models.Transaction.objects.filter(
                status=wallets_models.Transaction.STATUS.unspent).annotate(
                total_btc=self.wallet_service.convert_satoshi_to_btc(Sum('amount')),
                total_usd=self.wallet_service.convert_satoshi_to_usd(Sum('amount'))), to_attr="balances"))
        return wallet_qs


class TransactionQuerySet(QuerySet):
    wallet_service = WalletService

    def all_for_user(self, user):
        return self.filter(Q(sender__creator=user) | Q(receiver__creator=user))

    def get_statistics(self):
        profit_satoshi = self.aggregate(total=Sum('platform_fee'))['total']
        profit_btc = self.wallet_service.convert_satoshi_to_btc(profit_satoshi)
        profit_usd = self.wallet_service.convert_satoshi_to_usd(profit_satoshi)
        total_transactions = wallets_models.Transaction.objects.count()
        return {
            'profit_satoshi': profit_satoshi,
            'profit_btc': profit_btc,
            'profit_usd': profit_usd,
            'total_transactions': total_transactions,
        }
