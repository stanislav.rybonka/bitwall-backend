from decimal import Decimal

import requests
from django.conf import settings
from requests import codes


class WalletService:
    @classmethod
    def convert_satoshi_to_btc(cls, total_satoshi):
        return total_satoshi * settings.SATOSHI_TO_BTC

    @classmethod
    def convert_satoshi_to_usd(cls, total_satoshi):
        btc_to_usd_last_price = cls.get_last_market_price(
            pair_of='BTC', pair_to='USD',
        )
        return cls.convert_satoshi_to_btc(total_satoshi) * btc_to_usd_last_price

    @classmethod
    def get_last_market_price(cls, pair_of, pair_to):
        api_url = settings.CEX_IO_API_LAST_MARKET_PRICE_URL
        response = requests.get(api_url.format(pair_of, pair_to))
        last_price = 0

        if response.status_code == codes.ok:
            data = response.json()
            last_price = data.get('lprice')

        return Decimal(last_price)
