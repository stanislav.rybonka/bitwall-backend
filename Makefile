tests:
	docker-compose run --rm app pytest

migrate:
	docker-compose run --rm app ./manage.py migrate